#pragma once
#include <gtkmm.h>

class Mods;
class ReaderView;

class Header : public Gtk::HeaderBar
{
public:
    Header(ReaderView *reader, Gtk::Window *window);
    virtual ~Header();

    void updateButtons(void);
    void updateMenus(void);
    ReaderView *reader;
    void showMods(void);
    void showText(void);

protected:
    //Buttons
    Gtk::Button back;
    Gtk::Button forward;
    Gtk::MenuButton book;
    Gtk::MenuButton menu;
    //Menus
    Gtk::PopoverMenu bookMenu;
    Gtk::PopoverMenu menuMenu;

    Mods *mods;
    Gtk::Window *window;
};
