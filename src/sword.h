#pragma once
#include <gtkmm.h>
#include <sword/swmodule.h>
#include <sword/swmgr.h>
#include <vector>

using namespace::std;

class Sword {
public:
    Sword();
    virtual ~Sword();

    std::vector<std::string> getModules(void);
    std::vector<std::string> getBooks(void);
    void setModule(std::string modName);
    void fillBuffer(std::string ref, Glib::RefPtr<Gtk::TextBuffer> buf);
    void getConfig(std::string ref, struct config *conf);

protected:
    sword::SWModule *target;
    sword::SWMgr library;
};

struct config {
    int chapter;
    std::string book;
    std::string bookFull;
    int maxChapter;
    std::string version;
};
