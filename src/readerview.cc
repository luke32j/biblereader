#include "readerview.h"
#include "sword.h"
#include "settings.h"

ReaderView::ReaderView()
: text()
{
    this->sword = new Sword();
    this->conf = new struct config;
    text.set_editable(false);
    text.set_cursor_visible(false);
    text.set_wrap_mode(Gtk::WrapMode::WRAP_WORD_CHAR);
    auto scroll = new Gtk::ScrolledWindow();
    scroll->add(text);
    add(*scroll);
    // Open the passage we had last time
    setPassage(settingsRead("passage"));
}

ReaderView::~ReaderView() {}

// Returns all new mods
std::vector<std::string> ReaderView::modsUpdated() {
    auto oldMods = getAllVersions();
    std::sort(oldMods.begin(), oldMods.end());
    sword = new Sword();
    auto newMods = getAllVersions();
    std::sort(newMods.begin(), newMods.end());
    std::vector<std::string> justNew;
    std::set_difference(newMods.begin(), newMods.end(), oldMods.begin(), oldMods.end(), std::back_inserter(justNew));
    refresh();
    return justNew;
}

void ReaderView::setPassage(std::string passage) {
    if(passage.empty()) {
        passage = "Gen 1";
    }
    sword->getConfig(passage, conf);
    refresh();
}

void ReaderView::refresh() {
    auto textBuffer = text.get_buffer();
    std::string passage = conf->book + " " + std::to_string(conf->chapter);
    //printf("Passage: %s\n", passage.c_str());
    settingsWrite("passage", passage);
    sword->getConfig(passage, conf);
    // Get passage back
    passage = conf->book + " " + std::to_string(conf->chapter);
    textBuffer->set_text(""); // Clear contents
    //auto iter = textBuffer->get_iter_at_offset(0);
    sword->fillBuffer(passage, textBuffer);
}

void ReaderView::setChapter(int chapter) {
    conf->chapter = chapter;
    refresh();
}

int ReaderView::getChapter() {
    return conf->chapter;
}

int ReaderView::getChapterMax() {
    return conf->maxChapter;
}

void ReaderView::setBook(std::string book) {
    conf->book = book;
    refresh();
}

std::string ReaderView::getBook() {
    return conf->book;
}

std::string ReaderView::getBookFull() {
    return conf->bookFull;
}

std::vector<std::string> ReaderView::getAllBooks() {
    return sword->getBooks();
}

void ReaderView::setVersion(std::string version) {
    sword->setModule(version);
    refresh();
}

std::string ReaderView::getVersion() {
    return conf->version;
}

std::vector<std::string> ReaderView::getAllVersions() {
    return sword->getModules();
}
