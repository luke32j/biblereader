#pragma once
#include <glib.h>
#include <thread>
#include <mutex>
#include <vector>
#include <string>
#include <gtkmm.h>
#include <sword/installmgr.h>
#include <sword/remotetrans.h>

using namespace std;

class Header;

/*
 * Credit goes to the Xiphos project for this part of the code:
 * https://github.com/crosswire/xiphos/
 */

class Mods : public Gtk::Frame, public sword::StatusReporter
{
public:
    Mods(Header *header, Gtk::Window *window);
    virtual ~Mods();

    void installMods(std::vector<std::string> filenames);
    void uninstallMods(std::vector<std::string> modnames);
    void updateInstallable();
    void update();
    void displayMain();
    void displayDownload();
    
    // This is for the StatusReporter. Huzzah for multiple inheritance!
    virtual void statusUpdate(double dltotal, double dlnow);
    virtual void preStatus(long totalBytes, long completedBytes, const char *message);

protected:
    std::string basedir;
    std::string confpath;
    Header *header;
    sword::InstallMgr *installMgr;
    Gtk::Window *window;
    std::map<std::string, std::vector<std::pair<sword::InstallSource *, sword::SWModule *>>> modsAvailable;

    Gtk::MessageDialog progressDialog;
    Gtk::ProgressBar progressBar;
    Glib::Dispatcher dispatcher;
    mutable std::mutex progressMutex;
    double fracDone;
    std::string message;
    std::thread *worker;
    bool complete;
    void getStatus(double *fractionDone, std::string *mess, bool *isComplete) const;
    void onNotify();

    void showProgress(std::string message);
    void endProgress();

};
