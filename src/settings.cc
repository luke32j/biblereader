#include "settings.h"
#include <glib.h>
#include <sword/swconfig.h>
#include <stdlib.h>

std::string path = (g_getenv("HOME")) + std::string("/.sword/biblereader.conf");
sword::SWConfig config(path.c_str());

void settingsWrite(std::string key, std::string value) {
    config["General"][key.c_str()] = sword::SWBuf(value.c_str());
    config.save();
}

std::string settingsRead(std::string key) {
    return config["General"][key.c_str()].c_str();
}

void settingsWriteInt(std::string key, int value) {
    config["General"][key.c_str()] = sword::SWBuf(std::to_string(value).c_str());
    config.save();
}

int settingsReadInt(std::string key) {
    return atoi(config["General"][key.c_str()].c_str());
}
