#include "sword.h"
#include <sword/versekey.h>
#include <sword/markupfiltmgr.h>
#include <sstream>
#include "settings.h"
#include <wctype.h>

using namespace sword;
using namespace std;

bool isNoMods;

Sword::Sword()
: library(new MarkupFilterMgr(FMT_XHTML))
{
    isNoMods = false;
    // Attempt to set module from last time
    std::vector<std::string> mods = getModules();
    std::string savedMod = settingsRead("module");
    if(std::find(mods.begin(), mods.end(), savedMod) != mods.end()) {
        setModule(savedMod);
    } else if(mods.size() > 0) {
        setModule(mods[0]);
        settingsWrite("module", mods[0]);
    } else {
        // Uh oh.
        isNoMods = true;
    }
}

Sword::~Sword() {}

vector<string> Sword::getModules() {
    vector<string> mods;
    ModMap::iterator it;
    for (it = library.Modules.begin(); it != library.Modules.end(); it++) {
        mods.push_back((*it).second->getName());
    }
    return mods;
}

void Sword::setModule(string modName) {
    this->target = library.getModule(modName.c_str());
    settingsWrite("module", modName);
}

const Glib::ustring spanIndent("<span class=\"line indent0\">");
const Glib::ustring spanDivine("<span class=\"divineName\">");
const Glib::ustring spanJesus("<span class=\"wordsOfJesus\">");
const std::vector<Glib::ustring> allSpans = {spanIndent, spanDivine, spanJesus};

const std::map<Glib::ustring, Glib::ustring> subs = {{"¶", "\n\t"}};

// True if spanJesus occurs before first printing non-whitespace character
bool isJesusVerse(Glib::ustring text) {
    bool inTag = false;
    int firstNonWhitespace = 0;
    for(auto i = text.begin(); i != text.end(); i++, firstNonWhitespace++) {
        if(*i == '<') {
            inTag = true;
        }
        if(! inTag && ! iswspace(*i)) {
            break;
        }
        if(*i == '>') {
            inTag = false;
        }
    }
    return (firstNonWhitespace > text.find(spanJesus));
}

void Sword::fillBuffer(string ref, Glib::RefPtr<Gtk::TextBuffer> buf) {
    buf->set_text(""); // Clear contents

    if(isNoMods) {
        auto iter = buf->get_iter_at_offset(0);
        iter = buf->insert_markup(iter, "<big><b>No modules installed.</b></big>\n");
        iter = buf->insert_markup(iter, "Please download some modules at:\n");
        iter = buf->insert_markup(iter, "\thttp://crosswire.org/sword/modules/ModDisp.jsp?modType=Bibles\n");
        iter = buf->insert_markup(iter, "Then install them using the menu in the upper right corner, or use the built-in installer to download and install modules.");
        return;
    }

    target->setKey(ref.c_str());
    auto iter = buf->get_iter_at_offset(0);

    auto verseSize = buf->create_tag();
    verseSize->property_size() = settingsReadInt("fontsize");
    if(verseSize->property_size() == 0) {
	verseSize->property_size() = 12000;
    }
    std::vector<Glib::RefPtr<Gtk::TextBuffer::Tag>> globalTags = {verseSize};

    auto verseScale = buf->create_tag();
    verseScale->property_scale() = 0.8;
    auto verseOffset = buf->create_tag();
    verseOffset->property_rise() = 3000;
    auto indent = buf->create_tag();
    indent->property_left_margin() = 40;
    auto redletter = buf->create_tag();
    redletter->property_foreground_gdk() = Gdk::Color("red");

    bool endOfParagraph = true;

    VerseKey *key = (VerseKey *) target->getKey();
    int curChap = key->getChapter();
    //std::cout << key->getChapterMax() << "\n";
    for(; key->getChapter() == curChap; (*key)++) {
        Glib::ustring text = Glib::ustring(target->renderText());

        // Handle ¶ symbol if at beginning of line
        if(text.find("¶") == 0) {
            text.erase(0, 1);
            iter = buf->insert(iter, "\n");
            endOfParagraph = true;
        }

        // Find and replace everything in "subs" map
        for(auto const &repl : subs) {
            Glib::ustring::size_type location;
            while((location = text.find(repl.first)) != Glib::ustring::npos) {
                text.replace(location, repl.first.size(), repl.second);
            }
        }

        std::vector<Glib::RefPtr<Gtk::TextBuffer::Tag>> verseTags(globalTags.begin(), globalTags.end());
        verseTags.push_back(verseScale);
        verseTags.push_back(verseOffset);
        if(text.find(spanIndent) == Glib::ustring::npos || text.find(spanIndent) > 1) {
            if(endOfParagraph) {
                iter = buf->insert(iter, "\t");
            } else {
                iter = buf->insert(iter, " ");
            }
        } else {
            verseTags.push_back(indent);
        }
        if(isJesusVerse(text)) {
            verseTags.push_back(redletter);
        }
        // Insert verse number
        iter = buf->insert_with_tags(iter, std::to_string(key->getVerse()), verseTags);

        // Variable to accumulate unterminated spans
        std::vector<Glib::ustring> spans;
        // Don't print whitespace until printing other stuff
        bool hasPrinted = false;
        // Iterate over text
        for(auto i = text.begin(); i != text.end(); i++) {
            if(*i != '<') {
                if(!iswspace(*i) || hasPrinted) {
                    hasPrinted = true;
                    const char ch = *i;
                    Glib::ustring strin;
                    strin.push_back(*i);
                    iter = buf->insert_with_tags(iter, strin, globalTags);
                }
            }
            else {
                Glib::ustring span;
                for(; i != text.end(); i++) {
                    span.push_back(*i);
                    if(*i == '>') {
                        break;
                    }
                }
                if(span.find("span") != Glib::ustring::npos) {
                    spans.push_back(span);
                    while(! spans.empty()) {
                        // Collect text until we find "</span>".
                        Glib::ustring spanned;
                        for(i++; *i != '<'; i++) {
                            spanned.push_back(*i);
                            hasPrinted = true; // technically not yet, but whatever.
                        }
                        Glib::ustring unspan;
                        for(; *i != '>'; i++) {
                            unspan.push_back(*i);
                        }
                        unspan.push_back('>');
                        std::vector<Glib::RefPtr<Gtk::TextBuffer::Tag>> spanTags(globalTags.begin(), globalTags.end());
                        if(std::find(spans.begin(), spans.end(), spanIndent) != spans.end()) {
                            spanTags.push_back(indent);
                        }
                        if(std::find(spans.begin(), spans.end(), spanJesus) != spans.end()) {
                            spanTags.push_back(redletter);
                        }
                        // Potentially push other tags...

                        //Smallcaps is a hackjob. It requires speshul attention.
                        if(std::find(spans.begin(), spans.end(), spanDivine) != spans.end()) {
                            auto tag = buf->create_tag();
                            //tag->property_variant() = Pango::VARIANT_SMALL_CAPS;
                            // There's no small caps support. Sigh. We do fake small caps instead.
                            // Because i lazy, first letter is normal caps and rest small caps, always.
                            spanned = spanned.uppercase();
                            iter = buf->insert_with_tags(iter, spanned.substr(0, 1), spanTags);
                            tag->property_scale() = 0.75;
                            spanTags.push_back(tag);
                            iter = buf->insert_with_tags(iter, spanned.substr(1), spanTags);
                        } else {
                            iter = buf->insert_with_tags(iter, spanned, spanTags);
                        }

                        //Did we miss any?
                        for(auto sp : spans) {
                            if(std::find(allSpans.begin(), allSpans.end(), sp) == allSpans.end()) {
                                std::cout << "WARN: Unrecognized span " << sp << "\n";
                            }
                        } // TODO: Consider reworking if there are additional spans.
                        // Current method is fine for 3 spans, but TERRIBLE for many
                        // supported spans (in terms of length of this method, time
                        // complexity, length of this comment, etc.)

                        if(unspan != "</span>") {
                            // nuther level of unspan...
                            spans.push_back(unspan);
                        } else {
                            spans.pop_back();
                        }
                    }
                }
            }
        }
        endOfParagraph = (text[text.length()-1] == '\n');
    }
}

void Sword::getConfig(string ref, struct config *conf) {
    if(isNoMods) {
        conf->chapter = 0;
        conf->book = "";
        conf->bookFull = "";
        conf->maxChapter = 0;
        conf->version = "";
    } else {
        if(ref.size() == 2) {
            printf("WARN: Got ref = %s, size = %d\n", ref.c_str(), ref.size());
            ref = "Gen 1";
        }
        target->setKey(ref.c_str());
        VerseKey *key = (VerseKey *) target->getKey();
        conf->chapter = key->getChapter();
        conf->book = std::string(key->getBookAbbrev());
        conf->bookFull = std::string(key->getBookName());
        conf->maxChapter = key->getChapterMax();
        conf->version = std::string(target->getName());
    }
}

vector<string> Sword::getBooks() {
    vector<string> books;
    if(isNoMods) {
        return books;
    }
    VerseKey *key = (VerseKey *) target->getKey();
    for(char t = 1; t <= key->getTestamentMax(); t++) {
        key->setTestament(t);
        for(char b = 1; b <= key->getBookMax(); b++) {
            key->setBook(b);
            // Bug (whose fault??) in JPS; they CLAIM to have two testaments,
            // but they only have one, which causes repeats.
            if(std::find(books.begin(), books.end(), key->getBookName()) == books.end()) {
                books.push_back(key->getBookName());
            }
        }
    }
    return books;
}
