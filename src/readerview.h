#pragma once
#include <gtkmm.h>

class Sword;

class ReaderView : public Gtk::Frame
{
public:
    ReaderView();
    virtual ~ReaderView();

    void setPassage(std::string passage);
    void setChapter(int chapter);
    int getChapter(void);
    int getChapterMax(void);
    void setBook(std::string book);
    std::string getBook(void);
    std::string getBookFull(void);
    std::vector<std::string> getAllBooks(void);

    void setVersion(std::string version);
    std::string getVersion(void);
    std::vector<std::string> getAllVersions(void);

    void refresh(void);

    std::vector<std::string> modsUpdated(void);

protected:
    Sword *sword;
    Gtk::TextView text;
    struct config *conf;
};
